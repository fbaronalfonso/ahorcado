function randomNumber(min, max){
    return Math.round(Math.random() * (max - min) + min);
}
var words = ["secreto", "vidrio", "MORA", "UVA", "SANDIA", "MARACUYA", "CHEVROLET", "TOYOTA", "RENAULT", "CASA", "TENEDOR", "CUCHARA", "CUCHILLO", "RESTAURANTE", "ASADERO", "PERRERA", "BOGOTA", "MEDELLIN", "CARRO"];
var randomWord = "";
var letter = [""];
var discoverWord = [];
var availableLetters = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","Ñ","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
var stepDraw = [ ` `,
    `
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;________<br>
        &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        _____________<br>
    `,
    `
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;________<br>
        &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp; O&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        _____________<br>
    `,
    `
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;________<br>
        &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        _____________<br>
    `,
    `
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;________<br>
        &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;/ | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        _____________<br>
    `,
    `
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;________<br>
        &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;/ | \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        _____________<br>
    `,
    `
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;________<br>
        &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;/ | \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;/ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        _____________<br>
    `,
    `
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;________<br>
        &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;/ | \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        &nbsp;&nbsp;&nbsp;/ \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
        _____________<br>
    `,
    `    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;________<br>
    &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
    &nbsp;&nbsp;&nbsp;&nbsp;O&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
    &nbsp;&nbsp;/ | \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
    &nbsp;&nbsp;&nbsp;/ \\&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|<br>
    _____________<br>

    X X X AHORCADO X X X
`,


]
var step = 0;
var availableGame = false;

document.getElementById('canvas').innerHTML = stepDraw[step];

main();

function drawBadLetter() {
    step++;
    console.log(step);
    document.getElementById('canvas').innerHTML = stepDraw[step];
}

function main() {
    chooseWord(); // todo: #2
    drawWordSpaces(); // todo #3
    availableGame = true;
}

function checkCompleteWord(found) {
    found = false;
    discoverWord.forEach(letter => {
        if (letter.trim() === '_') {
            found = true;
        }
    });
    return !found;
}

function readTheLetter($event) {
    var readLetter = $event.innerHTML.trim();
    availableLetters = availableLetters.filter(letter => {
        return letter !== readLetter;
    });
    $event.disabled = true;
    return readLetter;
}

function checkLetterIsOnWord(readLetter) {
    var found = false;
    for (const letter of randomWord) {
        //console.log(letter);
        if (letter.toUpperCase() === readLetter) {
            found = true;
        }
    }
    return found;
}

function playGame($event) {
    if(!availableGame) {
        return;
    }
    console.log($event);
    console.log($event.innerHTML);
    var readLetter = readTheLetter($event); // todo: #5
    var found = checkLetterIsOnWord(readLetter); // todo #6
    //console.log(found);
    if (found) {
        //console.log('entra')
        drawFoundLetter(readLetter); //todo #7
        found = checkCompleteWord(found); //todo #8
        if (found) {
            //console.log('GANA'); // todo #9
            document.getElementById('message').innerHTML = "GANASTE!!!";
            availableGame = false;
        }
    } else {
        drawBadLetter();// todo: #10
        if (step >= 8) { // todo #11
            //console.log("perdio!!!"); // todo #12
            document.getElementById('message').innerHTML = "PERDISTE!!! " + randomWord;

            availableGame = false;
        }
    }

}

function drawFoundLetter(readLetter) {
    var i = 0;
    for (const letter of randomWord) {
        //console.log(letter);
        if (letter.toUpperCase() === readLetter) {
            discoverWord[i] = readLetter + ' ';
        }
        i++;
    }

    drawWordSpaces();
}

function chooseWord(){
    randomWord = words [randomNumber( 0, 19 )];
    //console.log(randomWord);
    discoverWord = randomWord.split("");
    //console.log(discoverWord);
    discoverWord = discoverWord.map( letter => {
        return "_ ";
    });
    //console.log(discoverWord);

}

function drawWordSpaces(){

    wordSpace = document.getElementById("word-spaces");
    wordSpace.innerHTML = "";
    discoverWord.forEach(letter => {
        wordSpace.innerHTML = wordSpace.innerHTML + letter;
    });

}

